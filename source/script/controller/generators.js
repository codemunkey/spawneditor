"use strict";

const {
    ipcRenderer
} = require('electron');

const {
    dialog
} = require('electron').remote;

const fs = require('fs');

const lookup = require("../net/lifestoned");

var region = null;

module.exports = exports = {
    init: () => {
        loadDataFile('region.json', 'json').then((data) => {
            region = data;
            reloadEncounters();
        });

        $('#groupName').change(function (evt) {
            var text = $(this).val();
            var selected = $('#generators .active');
            var key = selected.data('key');
            selected.text(text);
            region.encounters.find(x => x.key == key).desc = text;
        });

        for (var i = 0; i < 16; i++) {
            let idx = i;
            $(`#gen_${idx.toString(16)}`).change(function (evt) {
                var key = parseInt($('#generators .active').data('key'));
                var enc = region.encounters[key];
                enc.value[idx] = parseInt($(this).val());
            });
        }
    },

    activate: () => {
        reloadEncounters();
    },

    deactivate: () => {

    },

    saveAs: () => {
        regionSave();
    }
};

function reloadEncounters() {
    if (!region) return;

    var dd = $('#generators');

    dd.children().remove();
    dd.append($('<li value="0">None</li>'));

    $.each(region.encounters, function(key, val) {
        var item = $('<li></li>')
            .text(val.desc || val.key)
            .data('key', val.key)
            .click(function(evt) {
                $('#generators .active').toggleClass('active');
                $(this).toggleClass('active');
                updateGenList($(this).data('key'));
            });
        dd.append(item);
    });
}

function updateGenList(key) {
    key = parseInt(key);
    var enc = region.encounters[key];
    //console.log(key, enc);

    $('#groupName').val(enc.desc);
    enc.value.forEach((v, k) => $(`#gen_${k.toString(16)}`).val(v));
}

function regionSave() {
    dialog.showSaveDialog(
        {
            filters : [
                { name : 'JSON Files', extensions: [ 'json' ] },
                { name : 'All Files', extensions: [ '*' ] }
            ],
        },
        function(file) {
            if (file) {
                fs.writeFile(file, JSON.stringify(map.getRegion()));
            }
        }
    );
}