"use strict";

const {
    app,
    BrowserWindow
} = require('electron');

const path = require('path');
const url = require('url');

module.exports = exports = function(appTitle) {
    let self = this;

    self.bw = new BrowserWindow({ title: appTitle, width: 1024, height: 768, show: false });
    // backgroundColor: ...
    //self.bw.webContents.openDevTools();

    self.bw.once('ready-to-show', () => { self.bw.show(); });
    self.bw.on('page-title-updated', (event, title) => {
        if (appTitle && title && !title.startsWith(appTitle)) {
            self.bw.setTitle(appTitle + ' : ' + title);
            event.preventDefault();
        }
    });

    self.load = (filepath) => {
        let fullpath = path.join(__dirname, filepath);

        console.log('attempting to load %s', fullpath);
        self.bw.loadURL(url.format({
            protocol: 'file:',
            slashes: true,
            pathname: fullpath
        }))
    };

};