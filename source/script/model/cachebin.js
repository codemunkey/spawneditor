'use strict;'

const zlib = require('zlib');

class BinReader {
    constructor(buffer) {
        this.buffer = buffer;
        this.view = new DataView(buffer);
        this.position = 0;
        this.encoder = new TextDecoder();
    }

    _incRead(fn, cnt) {
        //var tmp =
        try {
            var args = [ this.position ];
            if (cnt > 1) args.push(true);
            var res = fn.apply(this.view, args);
            this.position += cnt;
            return res;
        } catch (error) {
            console.log('read fail', this, error, error.stack);
            throw error;
        }
    }

    align() {
        var offset = this.position % 4;
        if (offset !== 0)
            this.position += 4 - offset;
    }

    getString() {
        var len = this.getInt16();
        var res = this.encoder.decode(new Uint8Array(this.buffer, this.position, len));
        this.position += len;
        return res;
    }

    getInt8() { return this._incRead(DataView.prototype.getInt8, 1); }
    getUint8() { return this._incRead(DataView.prototype.getUint8, 1); }
    getInt16() { return this._incRead(DataView.prototype.getInt16, 2); }
    getUint16() { return this._incRead(DataView.prototype.getUint16, 2); }
    getInt32() { return this._incRead(DataView.prototype.getInt32, 4); }
    getUint32() { return this._incRead(DataView.prototype.getUint32, 4); }
    getFloat() { return this._incRead(DataView.prototype.getFloat32, 4); }
    getDouble() { return this._incRead(DataView.prototype.getFloat64, 8); }

    getSingle() { return this.getFloat(); }

    getPackedInt16() {
        var tmp = this.getUint8();
        if ((tmp & 0x80) !== 0) {
            tmp = (tmp & 0x7f) << 8;
            tmp |= this.getUint8();
        }
        return tmp;
    }

    getPackedInt32() {
        var b0 = this.getUint8();
        var result = b0;
        if ((b0 & 0x80) !== 0) {
            var b1 = this.getUint8();
            result = ((b0 & 0x7f) << 8) | b1;

            if ((b0 & 0x40) !== 0) {
                var s1 = this.getUint16();
                result = ((b0 & 0x3f) << 24) | (b1 << 16) | s1;
            }
        }

        return result;
    }

    getVector2() {
        return {
            x: this.getFloat(),
            y: this.getFloat()
        }
    }

    getVector3() {
        return {
            x: this.getFloat(),
            y: this.getFloat(),
            z: this.getFloat()
        }
    }

    getQuaternion() {
        return {
            w: this.getFloat(),
            x: this.getFloat(),
            y: this.getFloat(),
            z: this.getFloat()
        }
    }

    getPosition() {
        return {
            objcell_id: this.getUint32(),
            origin: this.getVector3(),
            rotation: this.getQuaternion()
        };
    }

    getMany(ctor, cnt) {
        var res = null;
        try {
            cnt = cnt !== undefined ? cnt : this.getInt32();
            res = new Array(cnt);
            for (var i = 0; i < cnt; i++)
                res[i] = ctor(this);

            return res;

        } catch (error) {
            //console.log(res);
            throw error;
        }
    }

    getInt8Array(cnt) {
        cnt = cnt !== undefined ? cnt : this.getInt32();
        var res = new Int8Array(this.buffer, this.position, cnt);
        this.position += res.byteLength;
        return res;
    }
    getUint8Array(cnt) {
        cnt = cnt !== undefined ? cnt : this.getInt32();
        var res = new Uint8Array(this.buffer, this.position, cnt);
        this.position += res.byteLength;
        return res;
    }

    getInt16Array(cnt) {
        var cntp = cnt;
        cnt = cnt !== undefined ? cnt : this.getInt32();
        try {
            var tmp = new Uint8Array(this.buffer, this.position, cnt * 2);
            var res = new Int16Array(tmp.buffer, 0, cnt);
            this.position += tmp.byteLength;
            return res;
        } catch (error) {
            console.log('getInt16Array', cntp, cnt, error, this);
        }
    }
    getUint16Array(cnt) {
        cnt = cnt !== undefined ? cnt : this.getInt32();
        var res = new Uint16Array(this.buffer, this.position, cnt);
        this.position += res.byteLength;
        return res;
    }

    getInt32Array(cnt) {
        cnt = cnt !== undefined ? cnt : this.getInt32();
        var res = new Int32Array(this.buffer, this.position, cnt);
        this.position += res.byteLength;
        return res;
    }
    getUint32Array(cnt) {
        cnt = cnt !== undefined ? cnt : this.getInt32();
        var res = new Uint32Array(this.buffer, this.position, cnt);
        this.position += res.byteLength;
        return res;
    }
}

class CacheBinEntry {
    constructor(reader) {
        this.reader = reader;
        this.id = reader.getUint32();
        this.magic1 = reader.getUint32();
        this.magic2 = reader.getUint32();
        this.magic3 = reader.getUint32();
        this.type = reader.getUint8();

        if (this.type === 1) {
            // decompress
            this.compressedLength = reader.getUint32();
            this.compressed = reader.getUint8Array(this.compressedLength);
            this.uncompressedLength = reader.getUint32();
        }
    }

    decompress() {
        let result = zlib.inflateSync(this.compressed);
        return result;
    }

    decrypt() {
        let data = this.decompress();
        let buffer = new Uint32Array(data.buffer, 0, data.length >> 2);
        let packing = this.magic1;
        for (let i = 0; i < buffer.length; i++) {
            packing ^= this.magic2;
            packing += this.magic3;
            buffer[i] += (i + 1) * packing;
        }
        return data;
    }
}

let _buffer = null;
let _reader = null;
let _version = null;
let _count = null;
let _directory = {};

function getVersion() { return _version; }

function init(buffer) {
    _buffer = buffer.buffer;

    let size = buffer.length;
    data = new Uint32Array(_buffer, 0, size >> 2);
    let xor = size + (size << 15) >>> 0;
    for (let i = 0; i < data.length; i++) {
        data[i] ^= xor;
        xor <<= 3;
        xor += size;
    }

    _reader = new BinReader(_buffer);
    _version = _reader.getUint32();
    _count = _reader.getUint32();

    for (let i = 0; i < _count; i++) {
        let id = _reader.getUint32();
        let entry = new CacheBinEntry(_reader);

        console.log(`CacheBin File: ${id} :: ${entry.id}`);

        _directory[id] = entry;
    }
}

function getFile(id, m1, m2) {
    let data = _directory[id].decrypt();
    let magic1 = m1 >>> 0;
    let magic2 = m2 >>> 0;

    let buffer = new Uint32Array(data.buffer, 0, data.length >> 2);
    let packing = magic1;
    for (let i = 0; i < buffer.length; i++) {
        buffer[i] += (i + 1) * packing;
        packing ^= magic2;
    }

    let compressed = new Uint8Array(data.buffer, 4, data.length - 4);

    let result = zlib.inflateSync(compressed);
    return result;
}

module.exports = exports = {
    getVersion, init, getFile,

    getCellData: () => {
        let file = getFile(6, 0xcd57fd07, 0x697a2224);
        let reader = new BinReader(file.buffer);
        let blocks = {};

        let count = reader.getUint16();
        let buckets = reader.getUint16();

        for (let i = 0; i < count; i++) {
            let key = reader.getUint32();
            let block = null;

            let wcnt = reader.getUint32();
            let weenies = new Array(wcnt);
            for (let w = 0; w < wcnt; w++) {
                weenies[w] = {
                    wcid: reader.getUint32(),
                    pos: reader.getPosition(),
                    iid: reader.getUint32()
                };
            }

            let lcnt = reader.getUint32();
            let links = new Array(lcnt);
            for (let l = 0; l < lcnt; l++) {
                links[l] = {
                    source: reader.getUint32(),
                    target: reader.getUint32()
                };
            }

            blocks[key] = { weenies, links };
        }

        let traw = file.buffer.slice(reader.position);
        let terrain = new Uint16Array(traw, 0, traw.byteLength >> 1);
        console.log(traw, terrain);

        return { blocks, terrain };
    }
};
