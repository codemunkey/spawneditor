"use strict";

const {
    ipcRenderer
} = require('electron');

const {
    dialog
} = require('electron').remote;

const fs = require('fs');

const map = require("../ux/map");

//const lookup = require("../model/sheets");
const lookup = require("../net/lifestoned");
const cachebin = require("../model/cachebin");

var region = null;
var encounters = null;
var terrain = null;

ipcRenderer.on('menu-file-export-block', exportBlock);

module.exports = exports = {
    init: () => {
        //sheets.load('1ar4-UaV-E9TV63LEH70RW49iKvHmwlvOqYDbFuvUCuk', 'NEED API KEY');
        loadDataFile('region.json', 'json').then((data) => {
            region = data;
            map.setRegion(data);
        });
        loadDataFile('encounters.json', 'json')
            .then((data) => {
                encounters = data;
                map.setEncounters(data);
            });
        loadBinDataFile('cache.bin').then((data) => {
            cachebin.init(data);
            let cell = cachebin.getCellData();
            terrain = cell.terrain;
            map.setTerrain(cell.terrain);
        });

        $('#spawns').on("change", function() {
            var val = parseInt(this.value);
            $('#groupName').val(map.getRegion().encounters[val].desc);
        });

        $('#groupName').change(function (evt) {
            var $spawns = $('#spawns');
            var selected = $spawns.val();
            var text = $(this).val();
            $spawns.children(`[value='${selected}']`).text(text);
            map.getRegion().encounters[selected].desc = text;
        });
    },

    activate: () => {
        if (region) map.setRegion(region);
        if (encounters) map.setEncounters(encounters);
        if (terrain) map.setTerrain(terrain);
        map.init(lookup.getName, lookup.getGeneratorList);
    },

    deactivate: () => {

    }
};

function exportBlock() {
    dialog.showSaveDialog(
        {
            filters : [
                { name : 'SQL Files', extensions: [ 'sql' ] },
                { name : 'All Files', extensions: [ '*' ] }
            ],
        },
        function(file) {
            if (file) {
                let lb = map.getBlockId();
                let ri = region.encounterMap[lb];
                let { el, cache } = map.getBlockEncounters(lb);

                //console.log(region, lb, ri, el, cache);

                let vals = [];
                let now = new Date().toISOString();
                now = now.substr(0, now.length - 1); // mysql doesn't like the Z

                let enc = region.encounters[ri];
                //console.log(region.encounters, enc);

                for (let x = 0; x < 9; x++) {
                    for (let y = 0; y < 9; y++) {
                        //var idx = x * 9 + (8 - y);
                        var idx = x * 9 + y;
                        let eidx = cache ? (el[idx] >> 7) & 0xf : el[idx];
                        if (eidx === 0) continue;
                        vals.push(`(${lb}, ${enc.value[eidx]}, ${x}, ${y}, '${now}')`);
                    }
                }
                let del = `DELETE FROM \`encounter\` WHERE \`landblock\` = ${lb};\n\n`;
                let sql = "INSERT INTO `encounter` (`landblock`, `weenie_Class_Id`, `cell_X`, `cell_Y`, `last_Modified`) VALUES\n";
                sql += vals.join(",\n") + ";";

                fs.writeFileSync(file, del + sql);
            }
        }
    );

}
