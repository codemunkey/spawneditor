
var lsd_url = "https://lifestoned.org/"

module.exports = exports = {

    find: (weenieName) => {
        return new Promise((resolve, reject) => {
            $.post(lsd_url + "Weenie/WeenieFinder", {
                PartialName : weenieName
            }).then(function (data) {
                resolve(data);
            }).catch(function (status) {
                reject(status);
                //console.error('failed to fetch content: ', status);
            });
        });
    },

    getName: (weenieId) => {
        return new Promise((resolve, reject) => {
            if (!(weenieId in weenieNames)) {
                $.post(lsd_url + "Weenie/WeenieFinder", {
                    WeenieClassId : weenieId
                }).then(function (data) {
                    if (data) {
                        for (var i = 0; i < data.length; i++) {
                            var item = data[i];
                            if (item.WeenieClassId == weenieId) {
                                weenieNames[weenieId] = item.Name;
                                break;
                            }
                        }
                    }
                    resolve(weenieNames[weenieId] || weenieId);
                }).catch(function (status) {
                    reject(status);
                    //console.error('failed to fetch content: ', status);
                });
            } else {
                resolve(weenieNames[weenieId]);
            }
        });
    },

    getGeneratorList: (weenieId) => {
        return generatorItems[weenieId];
    }
};

var sheetId;
var weenieNames = {};
var generatorItems = {};