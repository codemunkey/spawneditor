
module.exports = exports = function(to, el, tplPath) {
	return new Promise((resolve, reject) => {
		var co = $(el);
		if (co.attr('current') == to) { return ;}

		$.get(tplPath + to.substr(1) + '.html').then(function (data) {
			var cc = co.clone();
			cc.html(data);
			cc.attr('current', to);
			co.replaceWith(cc);
			resolve(to);
		}).catch(function (status) {
			reject(status);
			//console.error('failed to fetch content: ', status);
		});
	});
};
